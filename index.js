// Operators

// Assignment Operators
	// Basic assignment operator -to assign a certain value to a variable (=)

	let assignmentNumber = 8;


	// Arithmetic assignment operators
		// Addition assignment operator (+=)
		assignmentNumber = assignmentNumber + 2;
		console.log("Result of Addition assignment operator: " + assignmentNumber);

		assignmentNumber += 2;
		console.log("Result of Addition assignment operator: " + assignmentNumber);

		// Subtraction assignment operator (-=)
		assignmentNumber -= 2;
		console.log("Result of Subtraction assignment operator: " + assignmentNumber);

		// Multiplication assignment operator (*=)
		assignmentNumber *= 2;
		console.log("Result of Multiplication assignment operator: " + assignmentNumber);

		// Division assignment operator (/=)
		assignmentNumber /= 2;
		console.log("Result of Division assignment operator: " + assignmentNumber);

		// Arithmetic Operators
		let x = 1397;
		let y = 7831;

		let sum = x + y;
		console.log("Result of addition operator: " + sum);

		let difference = x - y;
		console.log("Result of subtraction operator: " + difference);

		let product = x * y;
		console.log("Result of multiplication operator: " + product);

		let quotient = x / y;
		console.log("Result of division operator: " + quotient);

		let remainder = x % y;
		console.log("Result of remainder operator: " + remainder);

// Multiple Operators and Parentheses
	
	// Using MDAS
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas);

	// Using Parentheses
	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log('Result of pemdas operation: ' + pemdas);

	// Increment (++) and Decrement (--)
	let z = 1;

	// Pre-increment
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	// Post-increment
	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);

	// Pre-decrement
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	// Post-decrement
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);

	// Type coercion
	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = true + 1;
	console.log(numC);

	let numD = false + 1;
	console.log(numD);

	// Relational operators

	// Equality operator (==)

	let juan = 'juana';
	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log('juan' == juan);

	// Inequality operator (!=)
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log('juan' != juan); //true

	//  <, >, <=, >=
	console.log(4 < 6); //true
	console.log(2 > 8); //false
	console.log(5 > 5); //false
	console.log(10 <= 15); //true

	// Strict equality operator (===)
	console.log("Strict equality:");
	console.log(1 === '1');

	// Strict inequality operator (!==)
	console.log("Strict inequality:");
	console.log(1 !== '1');

	// Logical operators
	let isLegalAge = true;
	let isRegistered = false;

	// AND Operator (&&)
	// Returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Return of logical AND operator: " + allRequirementsMet);

	// OR Operator (||)
	// Return true if one of the operands is true
	let	someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);








